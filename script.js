class PartyGame {
  constructor() {

  /*This.tricks - Reprezentuje jednotlivé možnosti triků, které jsou přidány
   getElementById - nastavuje proměnné na DOM elementy 
   this.score a this.timeLimit - nastavuje počáteční hodnoty skóre a limitu času
   addEventListener - přiřazuje tyto 2 funkce k tlačítkům hádání triku a zahájení hry*/
    this.tricks = [         
      { name: 'tanec', description: 'Tancuje na hudbu', image: 'img/tanec.jpg' },
      { name: 'zpěv', description: 'Zpívá písničku', image: 'img/zpev.jpg' },
      { name: 'vyprávění vtipu', description: 'Vypráví vtip', image: 'img/vtip.jpg' },
      { name: 'pantomima', description: 'Pantomimicky vyjadřuje různé situace', image: 'img/pantomima.jpg' },
      { name: 'kouzlení', description: 'Provádí kouzla s hracími kartami', image: 'img/karty.jpg' },
      { name: 'akrobacie', description: 'Provádí akrobatické kousky', image: 'img/akrobacie.jpg' },
      { name: 'žonglování', description: 'Žongluje s míčky', image: 'img/zongl.jpg' },
      { name: 'virtuální realita', description: 'Hraje hry ve virtuální realitě', image: 'img/vr.jpg' },
      { name: 'streamování her', description: 'Streamuje hry na PC', image: 'img/stream.jpg' },
      { name: 'natáčení podcastů', description: 'Natáčí podcast', image: 'img/podcast.jpg' },
      { name: 'graffiti', description: 'Maluje graffiti', image: 'img/graffiti.jpg' },
      { name: 'breakdance', description: 'Tančí breakdance', image: 'img/breakdance.jpg' },
      { name: 'balancování', description: 'Balancuje na desce', image: 'img/balanc.jpg' },
      { name: 'step', description: 'Tančí step', image: 'img/step.jpg' },
      { name: 'parkour', description: 'Provádí parkourové triky', image: 'img/parkour.jpg' },
      { name: 'TikTok tance', description: 'Tančí TikTok tanečky', image: 'img/tiktok.jpg' },
      { name: 'beatbox', description: 'Vytváří zvuky a rytmy ústy', image: 'img/beatbox.jpg' },
      { name: 'světelná show', description: 'Provádí světelnou show', image: 'img/svetshow.jpg' }
    ];
    this.result = document.getElementById('vysledek');            
    this.startBtn = document.getElementById('start-tlacitko');
    this.guessBtn = document.getElementById('hadani-tlacitko');
    this.guessInput = document.getElementById('hadej-pole');
    this.partyTrickElem = document.getElementById('party-trik');  
    this.trickDescriptionElem = document.getElementById('popis-triku');
    this.trickImageElem = document.getElementById('trik-obrazek');
    this.scoreElem = document.getElementById('skore');
    this.timeElem = document.getElementById('cas'); 
    this.score = 0;                                                   
    this.timeLimit = 30;
    this.timer = null;
    this.startBtn.addEventListener('click', this.startGame.bind(this));     
    this.guessBtn.addEventListener('click', this.guessTrick.bind(this));
  }



/*Slouží k zahájení nové hry */
  startGame() {                                  
    const randomIndex = Math.floor(Math.random() * this.tricks.length);       //generuje náhodné číslo od 0 do počtu triků (náhodně vybraný trik)
    this.partyTrick = this.tricks[randomIndex].name;                          //ukládá název náhodně vybraného triku do proměnné
    this.trickDescription = this.tricks[randomIndex].description;             //ukládá popis triku do proměnné
    this.trickImage = this.tricks[randomIndex].image;                         //ukládá cestu k obrázku
    this.partyTrickElem.textContent = 'O jaký trik se jedná?';                //text aby se ptal hráče
    this.trickImageElem.src = this.trickImage;                                //zobrazení obrázku
    this.trickDescriptionElem.textContent = this.trickDescription;            //nastavuje popis do elementu
    this.trickImageElem.style.display = 'block';                              //zobrazuje obrázek triku 
    this.result.innerText = '';                                               //maže výsledek z předchozího kola
    this.guessInput.value = '';                                               //maže textové pole, kvůli novému tipu
    this.toggleButtons(true);                                                 //aktivace tlačítka
    this.resetTimer();                                                        //reset časovače
    this.startTimer();                                                        //spouštěč časovače
  }



  /*Vyhodnocuje hádaný trik hráčem 
    const guessedTrick - získává hodnotu ze vstupního pole
    const normalizedParty... - převádí skutečný název triku na malá písmena
    const distance - počítá vzdálenost mezi hádaným a skutečným trikem
    if.. - kontrola zda je trik shodný nebo ne a podle toho vyhodnocuje a přičítá/odečítá body*/
  guessTrick() {                              
    const guessedTrick = this.guessInput.value.toLowerCase();                 
    const normalizedPartyTrick = this.partyTrick.toLowerCase();               
    const distance = this.calculateLevenshteinDistance(guessedTrick, normalizedPartyTrick); 
    const threshold = 3; 

    if (guessedTrick === normalizedPartyTrick) {
      this.result.innerText = 'Uhodl jsi to správně! Je to ' + this.partyTrick + '!';
      this.updateScore(10);
      this.toggleButtons(false);
      clearInterval(this.timer);
    } else if (distance <= threshold) {
      this.result.innerText = 'Jsi velmi blízko! Ale to není správně.';
      this.updateScore(-5);
    } else {
      this.result.innerText = 'Omlouváme se, to není to.';
      this.updateScore(-5);
    }
  
  }


  /*Tato metoda slouží k aktualizaci skóre hráče v rámci hry
    this.score+= - zvyšuje aktuální skóre hráče o počet bodů předaný do metody
    this.scroeElem... - nastavuje textový prvek scorElem, aby zobrazoval aktuální skóre */
  updateScore(points) {                     
    this.score += points;                                            
    this.scoreElem.innerText = 'Skóre: ' + this.score;
  }


  /*Slouží ke spuštění časovače
    let timeRemaining - zde je uložená hodnota časového limitu
    thistimeElem.innerText - zobrazuje zbývající čas ve hře
    this.timer... - spouští časovač pomocí funkce setInterval()
    timeRemaining - snížení časovače o 1 sekundu
    if (Time...) - kontroluje, zda čas klesl na nulu nebo pod nulu */
  startTimer() {                            
    let timeRemaining = this.timeLimit;                             
    this.timeElem.innerText = 'Čas: ' + timeRemaining;              
    this.timer = setInterval(() => {                            
      timeRemaining -= 1;
      this.timeElem.innerText = 'Čas: ' + timeRemaining;
      if (timeRemaining <= 0) {                                  
        clearInterval(this.timer);
        this.result.innerText = 'Čas vypršel! Trik byl ' + this.partyTrick + '.';
        this.toggleButtons(false);                                
      }
    }, 1000);
  }



  /*Slouží k resetování časovače použitého ve hře
    Podmínka if - kontrolu, zda je časovač aktuálně aktivní
    clearInterval - když je časovač aktivní, tak zastaví jeho činnost pomocí clearInterval()
     this.timeElem.innerText - aktualize txt prvek, který zobrazuje zbývající čas, aby znov zobrazil počáteční hodnotu*/
  resetTimer() {                            
    if (this.timer) {                       
      clearInterval(this.timer);            
    }
    this.timeElem.innerText = 'Čas: ' + this.timeLimit;     
  }




/* Výpočet Levenshteinovy vzdálenosti mezi dvěma řetězci a a b
   Levenshteinova vzdálenost udává minimální počet úprav, 
   které je potřeba provést na jednom řetězci, aby se shodoval
   s druhým řetězcem. */
  calculateLevenshteinDistance(a, b) {      
    if (a.length === 0) return b.length;    
    if (b.length === 0) return a.length;

    const matrix = [];

    
    for (let i = 0; i <= b.length; i++) {
      matrix[i] = [i];  
    }

    for (let j = 0; j <= a.length; j++) {
      matrix[0][j] = j;
    }

    
    for (let i = 1; i <= b.length; i++) {
      for (let j = 1; j <= a.length; j++) {
        if (b.charAt(i - 1) === a.charAt(j - 1)) {
          matrix[i][j] = matrix[i - 1][j - 1];
        } else {
          matrix[i][j] = Math.min(
            matrix[i - 1][j - 1] + 1, 
            matrix[i][j - 1] + 1,     
            matrix[i - 1][j] + 1      
          );
        }
      }
    }

    return matrix[b.length][a.length];
  }


  /*Povolení nebo zakázání tlačítka pro hádání,
    na základě hodnoty parametru enabled */
  toggleButtons(enabled) {
    this.guessBtn.disabled = !enabled; 
  }
}


/*Vytvoření instance hry a spuštění hry.*/
const game = new PartyGame();
